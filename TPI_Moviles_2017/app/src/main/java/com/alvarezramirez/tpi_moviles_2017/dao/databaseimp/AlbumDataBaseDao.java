package com.alvarezramirez.tpi_moviles_2017.dao.databaseimp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alvarezramirez.tpi_moviles_2017.Model.Album;
import com.alvarezramirez.tpi_moviles_2017.dao.AlbumDao;
import com.alvarezramirez.tpi_moviles_2017.dao.PhotoDao;
import com.alvarezramirez.tpi_moviles_2017.dao.PhotoDaoFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by niicoor on 19/11/17.
 */

public class AlbumDataBaseDao implements AlbumDao {

    private FlickrSqlDataBaseHelper flickrSqlDataBaseHelper;

    public AlbumDataBaseDao(Context context) {
        this.flickrSqlDataBaseHelper = FlickrSqlDataBaseHelper.getInstance(context);
    }

    @Override
    public List<Album> getAlbums() {
        SQLiteDatabase db = flickrSqlDataBaseHelper.getReadableDatabase();
        Cursor cursor = db.query("album", null, null,null,null,null,null);
        List<Album> albums = null;
        if (cursor.isBeforeFirst()) {
            Album album;
            int indexIdAlbum;
            int indexSecret;
            int indexServer;
            int indexFarm;
            int indexTitle;
            int indexPrimary;
            albums = new ArrayList<>();
            indexIdAlbum =  cursor.getColumnIndex("idAlbum");
            indexSecret = cursor.getColumnIndex("secret");
            indexServer = cursor.getColumnIndex("server");
            indexFarm = cursor.getColumnIndex("farm");
            indexTitle = cursor.getColumnIndex("title");
            indexPrimary = cursor.getColumnIndex("primaryId");

            while (cursor.moveToNext()) {
                album = new Album();
                album.setId(cursor.getString(indexIdAlbum));
                album.setSecret(cursor.getString(indexSecret));
                album.setServer(cursor.getString(indexServer));
                album.setFarm(cursor.getString(indexFarm));
                album.setTitle(cursor.getString(indexTitle));
                album.setPrimary(cursor.getString(indexPrimary));
                albums.add(album);
            }
        }
        cursor.close();
        db.close();
        return albums;
    }

    @Override
    public boolean deleteAlbums() {
        SQLiteDatabase db = flickrSqlDataBaseHelper.getWritableDatabase();
        int updated = db.delete("album", null, null);
        db.close();
        return updated != 0;
    }

    @Override
    public int addAlbums(List<Album> albums) {

        //Se realiza el borrado de toda la BD al cargar un nuevo usuario
        //para no acumular demasiados datos
        this.deleteAlbums();
        SQLiteDatabase db = flickrSqlDataBaseHelper.getWritableDatabase();
        int lastIndex = -1;
        lastIndex = db.delete("photo",null,null);
        lastIndex = db.delete("commentary", null, null);
        for (Album album: albums) {
            ContentValues values = new ContentValues();
            values.put("idAlbum"    , album.getId());
            values.put("primaryId"    , album.getPrimary());
            values.put("secret"     , album.getSecret());
            values.put("server"     , album.getServer());
            values.put("farm"       , album.getFarm());
            values.put("title"      , album.getTitle());
            lastIndex = (int)db.insert("album", null, values);
        }
        db.close();
        return lastIndex;
    }
}
