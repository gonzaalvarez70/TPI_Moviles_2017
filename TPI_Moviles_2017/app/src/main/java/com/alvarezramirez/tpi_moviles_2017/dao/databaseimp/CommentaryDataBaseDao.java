package com.alvarezramirez.tpi_moviles_2017.dao.databaseimp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alvarezramirez.tpi_moviles_2017.Model.Commentary;
import com.alvarezramirez.tpi_moviles_2017.dao.CommentaryDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by niicoor on 18/11/17.
 */

public class CommentaryDataBaseDao implements CommentaryDao {

    private FlickrSqlDataBaseHelper flickrSqlDataBaseHelper;

    public CommentaryDataBaseDao(Context context) {
        this.flickrSqlDataBaseHelper = FlickrSqlDataBaseHelper.getInstance(context);
    }

    @Override
    public List<Commentary> getCommentaries(String idPhoto) {

        SQLiteDatabase db = flickrSqlDataBaseHelper.getReadableDatabase();
        Cursor cursor = db.query("commentary", null, null,null,null,null,null);
        List<Commentary> commentaries = null;
        if (cursor.isBeforeFirst()) {
            Commentary commentary;
//            int indexId;
            int indexUser;
            int indexDate;
            int indexCommentary;
            int indexIdPhoto;
            commentaries = new ArrayList<>();
//            indexId = cursor.getColumnIndex("id");
            indexUser = cursor.getColumnIndex("user");
            indexDate = cursor.getColumnIndex("date");
            indexIdPhoto = cursor.getColumnIndex("idPhoto");
            indexCommentary = cursor.getColumnIndex("commentary");

            while (cursor.moveToNext()) {
                if(cursor.getString(indexIdPhoto).compareTo(idPhoto)==0){
                    commentary = new Commentary();
                    commentary.setUser(cursor.getString(indexUser));
                    commentary.setDate(cursor.getString(indexDate));
                    commentary.setCommentary(cursor.getString(indexCommentary));
                    commentaries.add(commentary);
                }
            }
        }
        cursor.close();
        db.close();
        return commentaries;
    }

    @Override
    public boolean deleteCommentaries(String idPhoto) {
        SQLiteDatabase db = flickrSqlDataBaseHelper.getWritableDatabase();
        int updated = db.delete("commentary","WHERE idPhoto = "+idPhoto,null);
        db.close();
        return updated != 0;
    }

    @Override
    public int addCommentaries(List<Commentary> commentaries, String idPhoto) {
        //Para que no se acumulen comentarios de diferentes fotos.
       // this.deleteCommentaries();
        SQLiteDatabase db = flickrSqlDataBaseHelper.getWritableDatabase();
        int lastIndex = -1;
        for (Commentary commentary: commentaries) {
        ContentValues values = new ContentValues();
            values.put("user"       , commentary.getUser());
            values.put("date"       , commentary.getDate());
            values.put("commentary" , commentary.getCommentary());
            values.put("idPhoto", idPhoto);
        lastIndex = (int)db.insert("commentary", null, values);
        }
        db.close();
        return lastIndex;
    }



}
