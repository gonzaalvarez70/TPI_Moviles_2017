package com.alvarezramirez.tpi_moviles_2017.Services;

import com.alvarezramirez.tpi_moviles_2017.Model.Commentary;
import com.alvarezramirez.tpi_moviles_2017.dao.AlbumDao;
import com.alvarezramirez.tpi_moviles_2017.dao.CommentaryDao;
import com.alvarezramirez.tpi_moviles_2017.dao.PhotoDao;

/**
 * Created by niicoor on 19/11/17.
 */

public class StorageService {

    private CommentaryDao commentaryDao;
    private AlbumDao  albumDao;
    private PhotoDao photoDao;
    private static StorageService instance = new StorageService();

    public static StorageService getInstance() {
        return instance;
    }

    public void load(CommentaryDao commentaryDao, AlbumDao albumDao, PhotoDao photoDao) {
        if (this.commentaryDao == null) {
            this.commentaryDao = commentaryDao;
        }
        if (this.albumDao == null) {
            this.albumDao = albumDao;
        }
        if (this.photoDao == null) {
            this.photoDao = photoDao;
        }

    }

    public CommentaryDao getCommentaryDao() {

        return commentaryDao;
    }

    public AlbumDao getAlbumDao() {
        return albumDao;
    }

    public PhotoDao getPhotoDao() {
        return photoDao;
    }
}
