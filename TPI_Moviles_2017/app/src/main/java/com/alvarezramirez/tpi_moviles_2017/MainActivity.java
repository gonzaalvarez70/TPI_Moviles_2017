package com.alvarezramirez.tpi_moviles_2017;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alvarezramirez.tpi_moviles_2017.Entities.UserFlickr;
import com.alvarezramirez.tpi_moviles_2017.Services.FlickrApiService;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Locale;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;

public class MainActivity extends AppCompatActivity {
    private  FlickrApiService urlService;
    private Gson gson;
    private String userName;
    private UserFlickr userFlickr;
    private EditText editText;
    private MainActivity mainActivity;
    private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        urlService = MyApplication.getUrlService();
        editText = (EditText)findViewById(R.id.userNameText);
        Button button = (Button)findViewById(R.id.buscarUsuario);
        imageView = (ImageView)findViewById(R.id.logo);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
        mainActivity = this;
        /*Intent intent = new Intent(this, AlbumActivity.class);
        intent.putExtra("idUser","86423215@N00");
        intent.putExtra("idPhotoSet","72157677763280221");*/
        /*Intent intent = new Intent(this, UserPhotoSetActivity.class);
        intent.putExtra("idUser","86423215@N00");
        startActivity(intent);*/
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                userName = editText.getText().toString();
                mainActivity.loadUser(userName);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.setLocale();
        setContentView(R.layout.activity_main);
        editText = (EditText)findViewById(R.id.userNameText);
        Button button = (Button)findViewById(R.id.buscarUsuario);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                userName = editText.getText().toString();
                mainActivity.loadUser(userName);
            }
        });
        editText.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            userName = editText.getText().toString();
                            mainActivity.loadUser(userName);
                            return true;
                        }
                        return false;
                    }
                });
        Resources resources = getBaseContext().getResources();
        Configuration configuration = resources.getConfiguration();
        // Chequear el lenguaje actual en las configuraciones
        if (configuration.locale == Locale.ENGLISH) {
            Toast.makeText(this, "English", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Español", Toast.LENGTH_SHORT).show();
        }
    }


    //Metodo para cambiar el lenguaje cada vez que se inicia esta activity
    private void setLocale(){
        Locale locale;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String language = sharedPreferences.getString("listPref","1");
        int lang = Integer.parseInt(language);
        if(lang==1){
            locale = new Locale("es", "ES");
        }else{
           locale = Locale.ENGLISH;
        }
        Resources resources = getBaseContext().getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(locale);
        DisplayMetrics dm = resources.getDisplayMetrics();
        getBaseContext().createConfigurationContext(configuration);
        resources.updateConfiguration(configuration,dm);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.preferencesMenu:
            {
                Intent intent = new Intent(this, PreferenceFlickrActivity.class);
                startActivity(intent);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public  void loadUser(String userName){
        String url = urlService.getUserIdByUserNameURL(userName);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                onUserLoaded, onUserError);
        MyApplication.getSharedQueue().add(request);
    }

    private   Response.Listener<String> onUserLoaded = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Toast toast;
            userFlickr = gson.fromJson(response, UserFlickr.class);
            if(userFlickr.getStat().compareTo("ok") == 0){
                toast = Toast.makeText(MainActivity.this, "Usuario Encontrado", Toast.LENGTH_SHORT);
                toast.show();
                Intent intent = new Intent(getBaseContext(), UserPhotoSetActivity.class);
                intent.putExtra("idUser",userFlickr.getUser().getNsid());
                startActivity(intent);
            }else{
                toast = Toast.makeText(MainActivity.this, "Usuario inexistente", Toast.LENGTH_SHORT);
                toast.show();
            }

        }
    };

    private  Response.ErrorListener onUserError = new Response.ErrorListener(){

        @Override
        public void onErrorResponse(VolleyError error) {
            System.out.print(error.getMessage());
            Toast toast;
            toast = Toast.makeText(MainActivity.this, "Problema de conexión, se mostrarán los Albumes almacenados", Toast.LENGTH_SHORT);
            toast.show();
            Intent intent = new Intent(getBaseContext(), UserPhotoSetActivity.class);
            intent.putExtra("idUser","");
            startActivity(intent);

        }
    };


}
