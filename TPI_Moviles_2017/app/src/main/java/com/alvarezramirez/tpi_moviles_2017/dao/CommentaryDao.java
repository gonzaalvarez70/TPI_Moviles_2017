package com.alvarezramirez.tpi_moviles_2017.dao;

import com.alvarezramirez.tpi_moviles_2017.Model.Commentary;

import java.util.List;

/**
 * Created by niicoor on 18/11/17.
 */

public interface CommentaryDao {

    public List<Commentary> getCommentaries(String idPhoto);

    public boolean deleteCommentaries(String idPhoto);

    public int addCommentaries(List<Commentary> commentaries,String idPhoto);

}
