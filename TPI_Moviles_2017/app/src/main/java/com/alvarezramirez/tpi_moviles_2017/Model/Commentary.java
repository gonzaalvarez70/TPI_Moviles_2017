package com.alvarezramirez.tpi_moviles_2017.Model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by niicoor on 16/11/17.
 */

public class Commentary {

        private String user;
        private String commentary;
        private String date; //"dd/MM/yyyy" format

    public Commentary() {

    }

        private String formatDate(Long longDate)
        {
            Date date = new Date(longDate);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String sDate= sdf.format(date);
            return sDate;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getCommentary() {
            return commentary;
        }

        public void setCommentary(String commentary) {
            this.commentary = commentary;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {

            this.date = date;
        }

        public void setDateFromTimeStamp(String timeStamp){
            this.date = formatDate(Long.parseLong(timeStamp) * 1000);
        }

}
