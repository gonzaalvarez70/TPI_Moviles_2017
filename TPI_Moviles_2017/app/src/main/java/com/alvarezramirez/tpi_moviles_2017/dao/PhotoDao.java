package com.alvarezramirez.tpi_moviles_2017.dao;

import com.alvarezramirez.tpi_moviles_2017.Model.Photo;

import java.util.List;

/**
 * Created by niicoor on 18/11/17.
 */

public interface PhotoDao {

    public List<Photo> getPhotos(String idPhotoSet);

    public boolean deletePhotos(String idPhotoSet);

    public int addPhotos(List<Photo> photos, String idPhotoSet);



}
