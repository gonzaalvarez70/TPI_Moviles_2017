package com.alvarezramirez.tpi_moviles_2017;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alvarezramirez.tpi_moviles_2017.Entities.AlbumPhotos;
import com.alvarezramirez.tpi_moviles_2017.Entities.AlbumPhotos.PhotosetBean.PhotoBean;
import com.alvarezramirez.tpi_moviles_2017.Model.Photo;
import com.alvarezramirez.tpi_moviles_2017.Services.FlickrApiService;
import com.alvarezramirez.tpi_moviles_2017.Services.StorageService;
import com.alvarezramirez.tpi_moviles_2017.dao.AlbumDao;
import com.alvarezramirez.tpi_moviles_2017.dao.AlbumDaoFactory;
import com.alvarezramirez.tpi_moviles_2017.dao.PhotoDao;
import com.alvarezramirez.tpi_moviles_2017.dao.PhotoDaoFactory;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AlbumActivity extends ListActivity {
    private String idUser, idPhotoSet;
    private FlickrApiService urlService;
    private Gson gson;
    private AlbumPhotos albumPhotos;
    private PhotosAdapter photosAdapter;
    private List<Photo> photos;
    private PhotoDao photoDao = StorageService.getInstance().getPhotoDao();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album);
        urlService = MyApplication.getUrlService();
        idUser = this.getIntent().getStringExtra("idUser");//
        idPhotoSet = this.getIntent().getStringExtra("idPhotoSet");//
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
        this.loadPhotos(idUser, idPhotoSet);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getBaseContext(), PhotoActivity.class);
                intent.putExtra("idPhoto", ((Photo) parent.getItemAtPosition(position)).getIdPhoto());
                intent.putExtra("secret", ((Photo) parent.getItemAtPosition(position)).getSecret());
                intent.putExtra("server", ((Photo) parent.getItemAtPosition(position)).getServer());
                intent.putExtra("farm", String.valueOf(((Photo) parent.getItemAtPosition(position)).getFarm()));
                startActivity(intent);
            }
        });

        Button buttonOrderByName = (Button) findViewById(R.id.orderByName);
        buttonOrderByName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (photos.size()!=0){
                    Collections.sort(photos, new Comparator<Photo>() {

                        public int compare(Photo photo2, Photo photo1)
                        {
                            return  photo2.getTitle().compareTo(photo1.getTitle());
                        }
                    });
//                    photosAdapter = new PhotosAdapter();
                    photosAdapter.setPhotos(photos);
                    setListAdapter(photosAdapter);
                    photosAdapter.notifyDataSetChanged();
                }
            }
        });

        Button buttonOrderByDate = (Button) findViewById(R.id.orderByDate);
        buttonOrderByDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (photos.size()!=0){
                    Collections.sort(photos, new Comparator<Photo>() {

                        public int compare(Photo photo2, Photo photo1)
                        {
                            return  photo1.getDate().compareTo(photo2.getDate());
                        }
                    });
//                    photosAdapter = new PhotosAdapter();
                    photosAdapter.setPhotos(photos);
                    setListAdapter(photosAdapter);
                    photosAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    private void loadPhotos(String idUser, String idPhotoSet){
        String url = urlService.getPhotoSet(idPhotoSet, idUser);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                onPhotoSetLoaded, onPhotoSetError);
        MyApplication.getSharedQueue().add(request);
    }
    private final Response.Listener<String> onPhotoSetLoaded = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Toast toast;
            albumPhotos = gson.fromJson(response, AlbumPhotos.class);
            photosAdapter = new PhotosAdapter();
            photos = parsePhotosetToPhotoFlickrList(albumPhotos);
            photosAdapter.setPhotos(photos);
            setListAdapter(photosAdapter);
            toast = Toast.makeText(AlbumActivity.this, "Se actualizarán las fotos almacenadas", Toast.LENGTH_SHORT);
            toast.show();
            int bdIndex = photoDao.addPhotos(photos,idPhotoSet);
            if (bdIndex != -1){
                toast = Toast.makeText(AlbumActivity.this, "Fotos almacenadas correctamente", Toast.LENGTH_SHORT);
                toast.show();
            }
            else
            {
                toast = Toast.makeText(AlbumActivity.this, "No se ha podido actualizar la Base de datos de Fotos", Toast.LENGTH_SHORT);
                toast.show();
            }
        }

    };

    private final Response.ErrorListener onPhotoSetError = new Response.ErrorListener(){

        @Override
        public void onErrorResponse(VolleyError error) {
            System.out.print(error.getMessage());
            Toast toast;
            toast = Toast.makeText(AlbumActivity.this, "No hay conexión. Se recuperará la info de fotos almacenados", Toast.LENGTH_SHORT);
            toast.show();
            List<Photo> photos = photoDao.getPhotos(idPhotoSet);
            if (photos.size()!= 0){
                photosAdapter = new PhotosAdapter();
                photosAdapter.setPhotos(photos);
                setListAdapter(photosAdapter);
                toast = Toast.makeText(AlbumActivity.this, "Info de Fotos recuperada Correctamente", Toast.LENGTH_SHORT);
                toast.show();
            }
            else{
               toast = Toast.makeText(AlbumActivity.this, "No hay fotos en base de datos", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    };

    private List<Photo> parsePhotosetToPhotoFlickrList(AlbumPhotos albumPhotos){

        List<PhotoBean> photos = albumPhotos.getPhotoset().getPhoto();

        List<Photo> photosFlickr = new ArrayList<>();
        Photo photoFlickr;
        int photoOrder = 1;
        for ( PhotoBean photo : photos) {
            photoFlickr = new Photo();
            photoFlickr.setTitle(photo.getTitle());
            photoFlickr.setFarm(String.valueOf(photo.getFarm()));
            photoFlickr.setIdPhoto(photo.getId());
            photoFlickr.setSecret(photo.getSecret());
            photoFlickr.setServer(photo.getServer());
            photoFlickr.setDate(String.valueOf(photoOrder));
            photoOrder++;

            photosFlickr.add(photoFlickr);
        }
        return photosFlickr;
    }

    private class PhotosAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {


        private ArrayList<Photo> list;
        private LayoutInflater inflater;

        public PhotosAdapter() {
            this.list = new ArrayList();
            this.inflater = LayoutInflater.from(getBaseContext());
        }

        public void setPhotos( List<Photo> photos){
            list.clear();
            list.addAll(photos);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View item = convertView != null ? convertView : inflater.inflate(R.layout.photo_item_view, null);
            Holder holder;
            if(item.getTag() == null){
                ImageView photo = (ImageView) item.findViewById(R.id.photoInAlbum);
                TextView title = (TextView) item.findViewById(R.id.titleInAlbum);
                holder = new Holder();
                holder.photo = photo;
                holder.title = title;
                item.setTag(holder);
            }
            else
            {
                holder = (Holder) item.getTag();
            }
            this.loadImage(position, holder.photo);

            holder.title.setText(((Photo) getItem(position)).getTitle());
            return item;

        }
        private void loadImage(int position, final ImageView photo){
            ImageLoader imageLoader = MyApplication.getImageLoader();
            String farm = ((Photo)getItem(position)).getFarm();
            String server =((Photo)getItem(position)).getServer();
            String idPhoto = ((Photo)getItem(position)).getIdPhoto();
            String secret = ((Photo)getItem(position)).getSecret();
            String url = urlService.getPhotoURL(farm,server,idPhoto,secret);
            imageLoader.get(url, new ImageLoader.ImageListener() {

                public void onErrorResponse(VolleyError error) {
                    System.out.print("Error: " + error.getMessage());
                }
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        photo.setImageBitmap(response.getBitmap());
                    }
                }
            });
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }

        private class Holder {
            public ImageView photo;
            public TextView title;
        }
    }
}
