package com.alvarezramirez.tpi_moviles_2017;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alvarezramirez.tpi_moviles_2017.Entities.PhotoComments;
import com.alvarezramirez.tpi_moviles_2017.Model.Commentary;
import com.alvarezramirez.tpi_moviles_2017.Services.FlickrApiService;
import com.alvarezramirez.tpi_moviles_2017.Services.StorageService;
import com.alvarezramirez.tpi_moviles_2017.dao.CommentaryDao;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class PhotoActivity extends ListActivity {

    private ImageView imageView;
    private FlickrApiService urlService;
    private Gson gson;
    private String idPhoto, secret, server, farm;
    private PhotoComments photoComments;
    private CommentsAdapter commentaryAdapter;
    private CommentaryDao commentaryDao = StorageService.getInstance().getCommentaryDao();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo);
        imageView = (ImageView)findViewById(R.id.imagephoto);
        idPhoto = this.getIntent().getStringExtra("idPhoto");
        secret = this.getIntent().getStringExtra("secret");
        server = this.getIntent().getStringExtra("server");
        farm = this.getIntent().getStringExtra("farm");
        urlService = MyApplication.getUrlService();
        final String staticPhotoUrl = urlService.getPhotoURL(farm,server,idPhoto,secret);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();


        Button buttonBrowser = (Button) findViewById(R.id.buttonBrowser);
        buttonBrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(staticPhotoUrl));
                startActivity(browserIntent);
            }
        });


        Button buttonEmail = (Button) findViewById(R.id.buttonSendMail);
        buttonEmail.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Foto de TPI_MOVILES_2017");
        intent.putExtra(Intent.EXTRA_TEXT   , "Hey mirá esta foto: "+ staticPhotoUrl);
        try {
            startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Log.e("ERROR", "There are no email clients installed.");
        }
            }
        });


        this.loadImage(staticPhotoUrl);
        this.loadComments(idPhoto);


    }

    private void loadComments(String idPhoto){
        String url = urlService.getPhotoComments(idPhoto);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                onCommentLoaded, onCommentError);
        MyApplication.getSharedQueue().add(request);

    }

    private final Response.Listener<String> onCommentLoaded = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Toast toast;
            photoComments = gson.fromJson(response, PhotoComments.class);
            commentaryAdapter = new CommentsAdapter();
            List<Commentary> comments = parsePhotoCommentsToCommentaryList(photoComments);
            commentaryAdapter.setComments(comments);
            setListAdapter(commentaryAdapter);
            //TODO: Informar que se va a actualizar la lista de comentarios de BD.
            toast = Toast.makeText(PhotoActivity.this, "Se actualizarán los comentarios almacenados", Toast.LENGTH_SHORT);
            toast.show();
            //TODO: Al recuperar el listado de comentarios de internet deberia ser actualizada la base de datos (eliminados datos anteriores y actualizar los nuevos)
            int bdIndex = commentaryDao.addCommentaries(comments,idPhoto);
            if (bdIndex != -1){
            //TODO: Informar que se ha terminado de actualizar la lista de comentarios de BD.
                toast = Toast.makeText(PhotoActivity.this, "Comentarios almacenados correctamente", Toast.LENGTH_SHORT);
                toast.show();
            }
            else
            {
                //TODO: Informar que no se ha podido actualizar la lista de comentarios de BD.
                toast = Toast.makeText(PhotoActivity.this, "No se ha podido actualizar la Base de datos de comentarios", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    };

    private final Response.ErrorListener onCommentError = new Response.ErrorListener(){

        @Override
        public void onErrorResponse(VolleyError error) {
            System.out.print(error.getMessage());
            Toast toast;
            //TODO: informar actualizacion de comentarios desde BD.
            toast = Toast.makeText(PhotoActivity.this, "No hay conexión. Se recuperarán los comentarios almacenados", Toast.LENGTH_SHORT);
            toast.show();
            //TODO: Al no recuperar los comentarios de internet deberia ser cargada la lista de comentarios con los almacenados en base de datos.
            List<Commentary> commentaries = commentaryDao.getCommentaries(idPhoto);
            if (commentaries.size()!= 0){
                commentaryAdapter = new CommentsAdapter();
                commentaryAdapter.setComments(commentaries);
                setListAdapter(commentaryAdapter);
                //TODO: informar que se recuperaron los datos de BD correctamente.
                toast = Toast.makeText(PhotoActivity.this, "Comentarios Recuperados Correctamente", Toast.LENGTH_SHORT);
                toast.show();
            }
            else{
                //TODO: informar si no llegaran a haber datos en BD.
                toast = Toast.makeText(PhotoActivity.this, "No hay comentarios en base de datos", Toast.LENGTH_SHORT);
                toast.show();
            }

        }
    };


    private void loadImage(String staticPhotoUrl){

        ImageLoader imageLoader = MyApplication.getImageLoader();
        String url = staticPhotoUrl;
        imageLoader.get(url, new ImageLoader.ImageListener() {

            public void onErrorResponse(VolleyError error) {
                System.out.print("Error: " + error.getMessage());
            }
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    imageView.setImageBitmap(response.getBitmap());
                }
            }
        });
    }


    private List<Commentary> parsePhotoCommentsToCommentaryList(PhotoComments photoComments){

        List<PhotoComments.CommentsBean.CommentBean> commentBean =photoComments.getComments().getComment();

        List<Commentary> comments = new ArrayList<>();
        Commentary commentary;
        for ( PhotoComments.CommentsBean.CommentBean comment : commentBean) {
            commentary = new Commentary();
            commentary.setUser(comment.getAuthorname());
            commentary.setDateFromTimeStamp(comment.getDatecreate());
            commentary.setCommentary(comment.get_content());
            comments.add(commentary);
        }
        return comments;
    }




     private class CommentsAdapter extends BaseAdapter{


        private ArrayList<Commentary> list;
        private LayoutInflater inflater;

        public CommentsAdapter() {
            this.list = new ArrayList();
            this.inflater = LayoutInflater.from(getBaseContext());
        }

         public void setComments( List<Commentary> comments){
            list.clear();
            list.addAll(comments);
        }

         @Override
         public int getCount() {
             return list.size();
         }

         @Override
         public Object getItem(int position) {
             return list.get(position);
         }

         @Override
         public long getItemId(int position) {
             return position;
         }

         @Override
         public View getView(int position, View convertView, ViewGroup parent) {
             View item = convertView != null ? convertView : inflater.inflate(R.layout.comment_item_view, null);
             Holder holder = null;
             if(item.getTag() == null){
                 TextView user = (TextView) item.findViewById(R.id.tw_name);
                 TextView date = (TextView) item.findViewById(R.id.tw_date);
                 TextView comment = (TextView) item.findViewById(R.id.tw_comment);
                holder = new Holder();
                 holder.user = user;
                 holder.date = date;
                 holder.comment = comment;
                 item.setTag(holder);
             }
             else
             {
                 holder = (Holder) item.getTag();
             }
             holder.user.setText(((Commentary)getItem(position)).getUser());
             holder.date.setText(((Commentary)getItem(position)).getDate());
             holder.comment.setText(((Commentary)getItem(position)).getCommentary());
             return item;

         }

         private class Holder {
             public TextView user;
             public TextView date;
             public TextView comment;
         }
     }



    }

