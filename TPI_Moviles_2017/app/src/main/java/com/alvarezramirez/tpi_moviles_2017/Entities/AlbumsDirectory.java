package com.alvarezramirez.tpi_moviles_2017.Entities;

import java.util.List;

/**
 * Created by gonza on 17/11/2017.
 */

public class AlbumsDirectory {

    /**
     * photosets : {"page":1,"pages":1,"perpage":32,"total":32,"photoset":[{"id":"72157680960482442","primary":"34612945801","secret":"fd3fabb8c7","server":"4267","farm":5,"photos":17,"videos":0,"title":{"_content":"Drone Shots"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":13,"count_comments":0,"can_comment":1,"date_create":"1494609815","date_update":"1510431050"},{"id":"72157677763280221","primary":"31644204034","secret":"3459849b91","server":"480","farm":1,"photos":4,"videos":0,"title":{"_content":"Hong Kong 2016"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":10,"count_comments":0,"can_comment":1,"date_create":"1485202844","date_update":"1497834465"},{"id":"72157671981770864","primary":"30200869510","secret":"fcb443d433","server":"5540","farm":6,"photos":28,"videos":0,"title":{"_content":"Bali 2016"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"104","count_comments":0,"can_comment":1,"date_create":"1477121059","date_update":"1499119197"},{"id":"72157671981770834","primary":"29852408394","secret":"de0476cfb4","server":"8623","farm":9,"photos":48,"videos":0,"title":{"_content":"Indonesia 2016"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":67,"count_comments":0,"can_comment":1,"date_create":"1477121059","date_update":"1501376879"},{"id":"72157658705596853","primary":"22720527130","secret":"f664271b25","server":"590","farm":1,"photos":8,"videos":0,"title":{"_content":"Oregon"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":18,"count_comments":0,"can_comment":1,"date_create":"1447115709","date_update":"1449955905"},{"id":"72157644924788776","primary":"14300004625","secret":"5de8b9f03e","server":"2918","farm":3,"photos":42,"videos":0,"title":{"_content":"Steptoe Butte"},"description":{"_content":"Trip to Steptoe Butte in eastern Washington. 2014"},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"194","count_comments":0,"can_comment":1,"date_create":"1401717385","date_update":"1467159631"},{"id":"72157640047710875","primary":"12089812213","secret":"92045c4598","server":"3756","farm":4,"photos":72,"videos":0,"title":{"_content":"Indonesia 2014"},"description":{"_content":"Photos from our 2014 trip to Indonesia."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"458","count_comments":0,"can_comment":1,"date_create":"1390415832","date_update":"1445993895"},{"id":"72157642561719555","primary":"12237140903","secret":"c8314e07df","server":"3745","farm":4,"photos":23,"videos":0,"title":{"_content":"Bali 2014"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"283","count_comments":0,"can_comment":1,"date_create":"1395182035","date_update":"1446737533"},{"id":"72157644068094841","primary":"13896950323","secret":"4322a0ddcf","server":"2885","farm":3,"photos":3,"videos":0,"title":{"_content":"Hong Kong 2014"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":22,"count_comments":0,"can_comment":1,"date_create":"1397836534","date_update":"1456445872"},{"id":"316475","primary":"4328096298","secret":"84c730bb4a","server":"2790","farm":3,"photos":"491","videos":0,"title":{"_content":"Lummi Island"},"description":{"_content":"Images from Lummi Island, WA."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"7735","count_comments":2,"can_comment":1,"date_create":"316475","date_update":"1510948143"},{"id":"72157629494984030","primary":"7096038123","secret":"0505354327","server":"7208","farm":8,"photos":50,"videos":0,"title":{"_content":"Bali 2012"},"description":{"_content":"The Bali portion of our trip to Indonesia."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"596","count_comments":0,"can_comment":1,"date_create":"1334926081","date_update":"1454279207"},{"id":"72157629485418586","primary":"7092119507","secret":"a0d91a0720","server":"5276","farm":6,"photos":82,"videos":0,"title":{"_content":"Indonesia 2012"},"description":{"_content":"Images from our trip to Indonesia from March/April 2012"},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"623","count_comments":1,"can_comment":1,"date_create":"1334799248","date_update":"1454279205"},{"id":"72157627710722679","primary":"6276614724","secret":"59b946720b","server":"6217","farm":7,"photos":95,"videos":0,"title":{"_content":"Foggy Bottom"},"description":{"_content":"Taken on foggy mornings on Lummi Island."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"558","count_comments":2,"can_comment":1,"date_create":"1317948888","date_update":"1506724704"},{"id":"1530997","primary":"540594456","secret":"d90330fcc3","server":"1182","farm":2,"photos":"1207","videos":0,"title":{"_content":"Pacific Northwest"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"4982","count_comments":1,"can_comment":1,"date_create":"1133971642","date_update":"1510948148"},{"id":"1323494","primary":"4176751566","secret":"fc9ce952ef","server":"4047","farm":5,"photos":"252","videos":0,"title":{"_content":"Seattle"},"description":{"_content":"Wandering around."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"2389","count_comments":0,"can_comment":1,"date_create":"1131465915","date_update":"1501284865"},{"id":"72157594454284514","primary":"361608445","secret":"74f4a28601","server":"145","farm":1,"photos":98,"videos":0,"title":{"_content":"Paris 2006"},"description":{"_content":"Paris 2006\n"},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"3050","count_comments":5,"can_comment":1,"date_create":"1167752058","date_update":"1397836611"},{"id":"72157632438438552","primary":"8053608534","secret":"67308236f9","server":"8309","farm":9,"photos":"135","videos":0,"title":{"_content":"Getty Selects"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"311","count_comments":0,"can_comment":1,"date_create":"1357346022","date_update":"1448193367"},{"id":"72157631511364067","primary":"7976262483","secret":"d90c17c1f3","server":"8180","farm":9,"photos":42,"videos":0,"title":{"_content":"Schooner Zodiac"},"description":{"_content":"Photos taken aboard the historic schooner Zodiac on a trip through the San Jaun Islands of Washington State."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"276","count_comments":0,"can_comment":1,"date_create":"1347371666","date_update":"1473205813"},{"id":"72157625950480132","primary":"5427796547","secret":"06cb655935","server":"5091","farm":6,"photos":50,"videos":0,"title":{"_content":"Bali 2011"},"description":{"_content":"From our 2011 trip to Bali. "},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"881","count_comments":2,"can_comment":1,"date_create":"1296526548","date_update":"1446493561"},{"id":"72157603989311282","primary":"2307891998","secret":"a10dbd08a0","server":"2331","farm":3,"photos":68,"videos":0,"title":{"_content":"Bali 2008"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"1851","count_comments":3,"can_comment":1,"date_create":"1204041101","date_update":"1468427339"},{"id":"72057594068263925","primary":"120301304","secret":"606d004ecc","server":44,"farm":1,"photos":"135","videos":0,"title":{"_content":"Bali 2006"},"description":{"_content":"Beautiful Bali."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"5584","count_comments":2,"can_comment":1,"date_create":"1140565294","date_update":"1412207884"},{"id":"308337","primary":"84871318","secret":"816221eb0d","server":39,"farm":1,"photos":69,"videos":0,"title":{"_content":"Bali 2005"},"description":{"_content":"Bali Indonesia."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"2676","count_comments":0,"can_comment":1,"date_create":"308337","date_update":"1510257202"},{"id":"308339","primary":"12243733","secret":"b5ac6a3730","server":11,"farm":1,"photos":28,"videos":0,"title":{"_content":"Bali 2004"},"description":{"_content":"Bali, Indonesia."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"1850","count_comments":1,"can_comment":1,"date_create":"308339","date_update":"1454631187"},{"id":"72157613322679603","primary":"3272308930","secret":"44873071ce","server":"3490","farm":4,"photos":22,"videos":0,"title":{"_content":"Kauai 2009"},"description":{"_content":"Kauai, Hawaii"},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"267","count_comments":1,"can_comment":1,"date_create":"1233764724","date_update":"1397836611"},{"id":"852512","primary":"38255752","secret":"0ba30e2168","server":33,"farm":1,"photos":64,"videos":0,"title":{"_content":"Alaska"},"description":{"_content":"Images from a 10 day trip aboard the &quot;Snow Goose&quot; from Sitka to Wrangell."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"966","count_comments":0,"can_comment":1,"date_create":"852512","date_update":"1509817667"},{"id":"72157603872362618","primary":"2263170556","secret":"d5ddf3032d","server":"2091","farm":3,"photos":26,"videos":0,"title":{"_content":"Hong Kong 2008"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"361","count_comments":0,"can_comment":1,"date_create":"1202513922","date_update":"1397836611"},{"id":"72157594180438455","primary":"177721098","secret":"c2b23d5c9a","server":62,"farm":1,"photos":14,"videos":0,"title":{"_content":"Hong Kong 2006"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"367","count_comments":0,"can_comment":1,"date_create":"1151506998","date_update":"1397836611"},{"id":"72157594163580259","primary":"165720204","secret":"26f4d7f577","server":60,"farm":1,"photos":11,"videos":0,"title":{"_content":"Eastern Washington"},"description":{"_content":"On the way to Coeur d' Alene in the Palouse area of Washington State."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"362","count_comments":1,"can_comment":1,"date_create":"1150123589","date_update":"1397836611"},{"id":"72157606198559746","primary":"2674349242","secret":"132b3999fe","server":"3295","farm":4,"photos":9,"videos":0,"title":{"_content":"Montana 2008"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"136","count_comments":0,"can_comment":1,"date_create":"1216214711","date_update":"1397836611"},{"id":"72157604154242485","primary":"2100273355","secret":"d2f49f8ca2","server":"2117","farm":3,"photos":7,"videos":0,"title":{"_content":"San Francisco"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":75,"count_comments":0,"can_comment":1,"date_create":"1205864446","date_update":"1397836611"},{"id":"72157610578357097","primary":"3076786577","secret":"43e635d2dc","server":"3194","farm":4,"photos":8,"videos":0,"title":{"_content":"Vancouver, B.C."},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":68,"count_comments":0,"can_comment":1,"date_create":"1228230591","date_update":"1423620903"},{"id":"72157626122321134","primary":"5471885870","secret":"f61e535e73","server":"5217","farm":6,"photos":16,"videos":0,"title":{"_content":"California"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":48,"count_comments":0,"can_comment":1,"date_create":"1298488390","date_update":"1500338604"}]}
     * stat : ok
     */

    private PhotosetsBean photosets;
    private String stat;

    public PhotosetsBean getPhotosets() {
        return photosets;
    }

    public void setPhotosets(PhotosetsBean photosets) {
        this.photosets = photosets;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public static class PhotosetsBean {
        /**
         * page : 1
         * pages : 1
         * perpage : 32
         * total : 32
         * photoset : [{"id":"72157680960482442","primary":"34612945801","secret":"fd3fabb8c7","server":"4267","farm":5,"photos":17,"videos":0,"title":{"_content":"Drone Shots"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":13,"count_comments":0,"can_comment":1,"date_create":"1494609815","date_update":"1510431050"},{"id":"72157677763280221","primary":"31644204034","secret":"3459849b91","server":"480","farm":1,"photos":4,"videos":0,"title":{"_content":"Hong Kong 2016"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":10,"count_comments":0,"can_comment":1,"date_create":"1485202844","date_update":"1497834465"},{"id":"72157671981770864","primary":"30200869510","secret":"fcb443d433","server":"5540","farm":6,"photos":28,"videos":0,"title":{"_content":"Bali 2016"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"104","count_comments":0,"can_comment":1,"date_create":"1477121059","date_update":"1499119197"},{"id":"72157671981770834","primary":"29852408394","secret":"de0476cfb4","server":"8623","farm":9,"photos":48,"videos":0,"title":{"_content":"Indonesia 2016"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":67,"count_comments":0,"can_comment":1,"date_create":"1477121059","date_update":"1501376879"},{"id":"72157658705596853","primary":"22720527130","secret":"f664271b25","server":"590","farm":1,"photos":8,"videos":0,"title":{"_content":"Oregon"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":18,"count_comments":0,"can_comment":1,"date_create":"1447115709","date_update":"1449955905"},{"id":"72157644924788776","primary":"14300004625","secret":"5de8b9f03e","server":"2918","farm":3,"photos":42,"videos":0,"title":{"_content":"Steptoe Butte"},"description":{"_content":"Trip to Steptoe Butte in eastern Washington. 2014"},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"194","count_comments":0,"can_comment":1,"date_create":"1401717385","date_update":"1467159631"},{"id":"72157640047710875","primary":"12089812213","secret":"92045c4598","server":"3756","farm":4,"photos":72,"videos":0,"title":{"_content":"Indonesia 2014"},"description":{"_content":"Photos from our 2014 trip to Indonesia."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"458","count_comments":0,"can_comment":1,"date_create":"1390415832","date_update":"1445993895"},{"id":"72157642561719555","primary":"12237140903","secret":"c8314e07df","server":"3745","farm":4,"photos":23,"videos":0,"title":{"_content":"Bali 2014"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"283","count_comments":0,"can_comment":1,"date_create":"1395182035","date_update":"1446737533"},{"id":"72157644068094841","primary":"13896950323","secret":"4322a0ddcf","server":"2885","farm":3,"photos":3,"videos":0,"title":{"_content":"Hong Kong 2014"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":22,"count_comments":0,"can_comment":1,"date_create":"1397836534","date_update":"1456445872"},{"id":"316475","primary":"4328096298","secret":"84c730bb4a","server":"2790","farm":3,"photos":"491","videos":0,"title":{"_content":"Lummi Island"},"description":{"_content":"Images from Lummi Island, WA."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"7735","count_comments":2,"can_comment":1,"date_create":"316475","date_update":"1510948143"},{"id":"72157629494984030","primary":"7096038123","secret":"0505354327","server":"7208","farm":8,"photos":50,"videos":0,"title":{"_content":"Bali 2012"},"description":{"_content":"The Bali portion of our trip to Indonesia."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"596","count_comments":0,"can_comment":1,"date_create":"1334926081","date_update":"1454279207"},{"id":"72157629485418586","primary":"7092119507","secret":"a0d91a0720","server":"5276","farm":6,"photos":82,"videos":0,"title":{"_content":"Indonesia 2012"},"description":{"_content":"Images from our trip to Indonesia from March/April 2012"},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"623","count_comments":1,"can_comment":1,"date_create":"1334799248","date_update":"1454279205"},{"id":"72157627710722679","primary":"6276614724","secret":"59b946720b","server":"6217","farm":7,"photos":95,"videos":0,"title":{"_content":"Foggy Bottom"},"description":{"_content":"Taken on foggy mornings on Lummi Island."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"558","count_comments":2,"can_comment":1,"date_create":"1317948888","date_update":"1506724704"},{"id":"1530997","primary":"540594456","secret":"d90330fcc3","server":"1182","farm":2,"photos":"1207","videos":0,"title":{"_content":"Pacific Northwest"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"4982","count_comments":1,"can_comment":1,"date_create":"1133971642","date_update":"1510948148"},{"id":"1323494","primary":"4176751566","secret":"fc9ce952ef","server":"4047","farm":5,"photos":"252","videos":0,"title":{"_content":"Seattle"},"description":{"_content":"Wandering around."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"2389","count_comments":0,"can_comment":1,"date_create":"1131465915","date_update":"1501284865"},{"id":"72157594454284514","primary":"361608445","secret":"74f4a28601","server":"145","farm":1,"photos":98,"videos":0,"title":{"_content":"Paris 2006"},"description":{"_content":"Paris 2006\n"},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"3050","count_comments":5,"can_comment":1,"date_create":"1167752058","date_update":"1397836611"},{"id":"72157632438438552","primary":"8053608534","secret":"67308236f9","server":"8309","farm":9,"photos":"135","videos":0,"title":{"_content":"Getty Selects"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"311","count_comments":0,"can_comment":1,"date_create":"1357346022","date_update":"1448193367"},{"id":"72157631511364067","primary":"7976262483","secret":"d90c17c1f3","server":"8180","farm":9,"photos":42,"videos":0,"title":{"_content":"Schooner Zodiac"},"description":{"_content":"Photos taken aboard the historic schooner Zodiac on a trip through the San Jaun Islands of Washington State."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"276","count_comments":0,"can_comment":1,"date_create":"1347371666","date_update":"1473205813"},{"id":"72157625950480132","primary":"5427796547","secret":"06cb655935","server":"5091","farm":6,"photos":50,"videos":0,"title":{"_content":"Bali 2011"},"description":{"_content":"From our 2011 trip to Bali. "},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"881","count_comments":2,"can_comment":1,"date_create":"1296526548","date_update":"1446493561"},{"id":"72157603989311282","primary":"2307891998","secret":"a10dbd08a0","server":"2331","farm":3,"photos":68,"videos":0,"title":{"_content":"Bali 2008"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"1851","count_comments":3,"can_comment":1,"date_create":"1204041101","date_update":"1468427339"},{"id":"72057594068263925","primary":"120301304","secret":"606d004ecc","server":44,"farm":1,"photos":"135","videos":0,"title":{"_content":"Bali 2006"},"description":{"_content":"Beautiful Bali."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"5584","count_comments":2,"can_comment":1,"date_create":"1140565294","date_update":"1412207884"},{"id":"308337","primary":"84871318","secret":"816221eb0d","server":39,"farm":1,"photos":69,"videos":0,"title":{"_content":"Bali 2005"},"description":{"_content":"Bali Indonesia."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"2676","count_comments":0,"can_comment":1,"date_create":"308337","date_update":"1510257202"},{"id":"308339","primary":"12243733","secret":"b5ac6a3730","server":11,"farm":1,"photos":28,"videos":0,"title":{"_content":"Bali 2004"},"description":{"_content":"Bali, Indonesia."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"1850","count_comments":1,"can_comment":1,"date_create":"308339","date_update":"1454631187"},{"id":"72157613322679603","primary":"3272308930","secret":"44873071ce","server":"3490","farm":4,"photos":22,"videos":0,"title":{"_content":"Kauai 2009"},"description":{"_content":"Kauai, Hawaii"},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"267","count_comments":1,"can_comment":1,"date_create":"1233764724","date_update":"1397836611"},{"id":"852512","primary":"38255752","secret":"0ba30e2168","server":33,"farm":1,"photos":64,"videos":0,"title":{"_content":"Alaska"},"description":{"_content":"Images from a 10 day trip aboard the &quot;Snow Goose&quot; from Sitka to Wrangell."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"966","count_comments":0,"can_comment":1,"date_create":"852512","date_update":"1509817667"},{"id":"72157603872362618","primary":"2263170556","secret":"d5ddf3032d","server":"2091","farm":3,"photos":26,"videos":0,"title":{"_content":"Hong Kong 2008"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"361","count_comments":0,"can_comment":1,"date_create":"1202513922","date_update":"1397836611"},{"id":"72157594180438455","primary":"177721098","secret":"c2b23d5c9a","server":62,"farm":1,"photos":14,"videos":0,"title":{"_content":"Hong Kong 2006"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"367","count_comments":0,"can_comment":1,"date_create":"1151506998","date_update":"1397836611"},{"id":"72157594163580259","primary":"165720204","secret":"26f4d7f577","server":60,"farm":1,"photos":11,"videos":0,"title":{"_content":"Eastern Washington"},"description":{"_content":"On the way to Coeur d' Alene in the Palouse area of Washington State."},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"362","count_comments":1,"can_comment":1,"date_create":"1150123589","date_update":"1397836611"},{"id":"72157606198559746","primary":"2674349242","secret":"132b3999fe","server":"3295","farm":4,"photos":9,"videos":0,"title":{"_content":"Montana 2008"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":"136","count_comments":0,"can_comment":1,"date_create":"1216214711","date_update":"1397836611"},{"id":"72157604154242485","primary":"2100273355","secret":"d2f49f8ca2","server":"2117","farm":3,"photos":7,"videos":0,"title":{"_content":"San Francisco"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":75,"count_comments":0,"can_comment":1,"date_create":"1205864446","date_update":"1397836611"},{"id":"72157610578357097","primary":"3076786577","secret":"43e635d2dc","server":"3194","farm":4,"photos":8,"videos":0,"title":{"_content":"Vancouver, B.C."},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":68,"count_comments":0,"can_comment":1,"date_create":"1228230591","date_update":"1423620903"},{"id":"72157626122321134","primary":"5471885870","secret":"f61e535e73","server":"5217","farm":6,"photos":16,"videos":0,"title":{"_content":"California"},"description":{"_content":""},"needs_interstitial":0,"visibility_can_see_set":1,"count_views":48,"count_comments":0,"can_comment":1,"date_create":"1298488390","date_update":"1500338604"}]
         */

        private int page;
        private int pages;
        private int perpage;
        private int total;
        private List<PhotosetBean> photoset;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getPerpage() {
            return perpage;
        }

        public void setPerpage(int perpage) {
            this.perpage = perpage;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<PhotosetBean> getPhotoset() {
            return photoset;
        }

        public void setPhotoset(List<PhotosetBean> photoset) {
            this.photoset = photoset;
        }

        public static class PhotosetBean {
            /**
             * id : 72157680960482442
             * primary : 34612945801
             * secret : fd3fabb8c7
             * server : 4267
             * farm : 5
             * photos : 17
             * videos : 0
             * title : {"_content":"Drone Shots"}
             * description : {"_content":""}
             * needs_interstitial : 0
             * visibility_can_see_set : 1
             * count_views : 13
             * count_comments : 0
             * can_comment : 1
             * date_create : 1494609815
             * date_update : 1510431050
             */

            private String id;
            private String primary;
            private String secret;
            private String server;
            private int farm;
            private int photos;
            private int videos;
            private TitleBean title;
            private DescriptionBean description;
            private int needs_interstitial;
            private int visibility_can_see_set;
            private int count_views;
            private int count_comments;
            private int can_comment;
            private String date_create;
            private String date_update;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPrimary() {
                return primary;
            }

            public void setPrimary(String primary) {
                this.primary = primary;
            }

            public String getSecret() {
                return secret;
            }

            public void setSecret(String secret) {
                this.secret = secret;
            }

            public String getServer() {
                return server;
            }

            public void setServer(String server) {
                this.server = server;
            }

            public int getFarm() {
                return farm;
            }

            public void setFarm(int farm) {
                this.farm = farm;
            }

            public int getPhotos() {
                return photos;
            }

            public void setPhotos(int photos) {
                this.photos = photos;
            }

            public int getVideos() {
                return videos;
            }

            public void setVideos(int videos) {
                this.videos = videos;
            }

            public TitleBean getTitle() {
                return title;
            }

            public void setTitle(TitleBean title) {
                this.title = title;
            }

            public DescriptionBean getDescription() {
                return description;
            }

            public void setDescription(DescriptionBean description) {
                this.description = description;
            }

            public int getNeeds_interstitial() {
                return needs_interstitial;
            }

            public void setNeeds_interstitial(int needs_interstitial) {
                this.needs_interstitial = needs_interstitial;
            }

            public int getVisibility_can_see_set() {
                return visibility_can_see_set;
            }

            public void setVisibility_can_see_set(int visibility_can_see_set) {
                this.visibility_can_see_set = visibility_can_see_set;
            }

            public int getCount_views() {
                return count_views;
            }

            public void setCount_views(int count_views) {
                this.count_views = count_views;
            }

            public int getCount_comments() {
                return count_comments;
            }

            public void setCount_comments(int count_comments) {
                this.count_comments = count_comments;
            }

            public int getCan_comment() {
                return can_comment;
            }

            public void setCan_comment(int can_comment) {
                this.can_comment = can_comment;
            }

            public String getDate_create() {
                return date_create;
            }

            public void setDate_create(String date_create) {
                this.date_create = date_create;
            }

            public String getDate_update() {
                return date_update;
            }

            public void setDate_update(String date_update) {
                this.date_update = date_update;
            }

            public static class TitleBean {
                /**
                 * _content : Drone Shots
                 */

                private String _content;

                public String get_content() {
                    return _content;
                }

                public void set_content(String _content) {
                    this._content = _content;
                }
            }

            public static class DescriptionBean {
                /**
                 * _content :
                 */

                private String _content;

                public String get_content() {
                    return _content;
                }

                public void set_content(String _content) {
                    this._content = _content;
                }
            }
        }
    }
}
