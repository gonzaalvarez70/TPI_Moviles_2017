package com.alvarezramirez.tpi_moviles_2017.dao;

import com.alvarezramirez.tpi_moviles_2017.Model.Album;

import java.util.List;

/**
 * Created by niicoor on 18/11/17.
 */

public interface AlbumDao {

    public List<Album> getAlbums();

    public boolean deleteAlbums();

    public int addAlbums(List<Album> albums);
}
