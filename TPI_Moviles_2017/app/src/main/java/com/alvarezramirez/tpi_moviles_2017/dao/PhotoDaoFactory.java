package com.alvarezramirez.tpi_moviles_2017.dao;

import android.content.Context;

import com.alvarezramirez.tpi_moviles_2017.dao.databaseimp.PhotoDataBaseDao;

/**
 * Created by niicoor on 18/11/17.
 */

public class PhotoDaoFactory {

        private static PhotoDataBaseDao photoDaoInstance;

        public static PhotoDataBaseDao createPhotoDaoInstance(Context context) {
            if (photoDaoInstance == null) {
               photoDaoInstance = new PhotoDataBaseDao(context);
            }
            return photoDaoInstance;
        }


    }
