package com.alvarezramirez.tpi_moviles_2017.Entities;

/**
 * Created by gonza on 19/11/2017.
 */

public class UserFlickr {

    /**
     * user : {"id":"155153998@N06","nsid":"155153998@N06","username":{"_content":"NiicooR"}}
     * stat : ok
     */

    private UserBean user;
    private String stat;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public static class UserBean {
        /**
         * id : 155153998@N06
         * nsid : 155153998@N06
         * username : {"_content":"NiicooR"}
         */

        private String id;
        private String nsid;
        private UsernameBean username;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNsid() {
            return nsid;
        }

        public void setNsid(String nsid) {
            this.nsid = nsid;
        }

        public UsernameBean getUsername() {
            return username;
        }

        public void setUsername(UsernameBean username) {
            this.username = username;
        }

        public static class UsernameBean {
            /**
             * _content : NiicooR
             */

            private String _content;

            public String get_content() {
                return _content;
            }

            public void set_content(String _content) {
                this._content = _content;
            }
        }
    }
}
