package com.alvarezramirez.tpi_moviles_2017.dao;

import android.content.Context;

import com.alvarezramirez.tpi_moviles_2017.dao.databaseimp.AlbumDataBaseDao;

/**
 * Created by niicoor on 18/11/17.
 */

public class AlbumDaoFactory {

    private static AlbumDao albumDao;

    public static AlbumDao createAlbumDaoInstance(Context context) {
        if (albumDao == null) {
            albumDao = new AlbumDataBaseDao(context);
        }
        return albumDao;
    }
}
