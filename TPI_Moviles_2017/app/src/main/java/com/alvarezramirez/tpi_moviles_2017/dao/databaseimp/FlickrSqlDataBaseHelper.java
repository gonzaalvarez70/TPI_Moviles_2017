package com.alvarezramirez.tpi_moviles_2017.dao.databaseimp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by niicoor on 18/11/17.
 */

public class FlickrSqlDataBaseHelper extends SQLiteOpenHelper {

    public static FlickrSqlDataBaseHelper instance;

    private static final String DATABASE_NAME = "FLICKRBD";
    private static final int VERSION = 1;

    private FlickrSqlDataBaseHelper(Context context) {
        super(context, DATABASE_NAME , null, VERSION);
    }

    public static synchronized FlickrSqlDataBaseHelper getInstance(Context context){
        if (instance == null){
            instance = new FlickrSqlDataBaseHelper(context);
        }
        return  instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE \"commentary\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT," +
                                                "\"user\" TEXT NOT NULL," +
                                                "\"date\" TEXT NOT NULL," +
                                                "\"idPhoto\" TEXT NOT NULL," +
                                                "\"commentary\" TEXT NOT NULL)");
        db.execSQL("CREATE TABLE \"photo\" (\"idPhoto\" TEXT PRIMARY KEY," +
                "\"secret\" TEXT NOT NULL," +
                "\"server\" TEXT NOT NULL," +
                "\"farm\" TEXT NOT NULL," +
                "\"title\" TEXT NOT NULL," +
                "\"date\" TEXT NOT NULL,"+
                "\"idAlbum\" TEXT NOT NULL)");

        db.execSQL("CREATE TABLE \"album\" (\"idAlbum\" TEXT PRIMARY KEY," +
                "\"secret\" TEXT NOT NULL," +
                "\"server\" TEXT NOT NULL," +
                "\"farm\" TEXT NOT NULL," +
                "\"title\" TEXT NOT NULL," +
                "\"primaryId\" TEXT NOT NULL)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
