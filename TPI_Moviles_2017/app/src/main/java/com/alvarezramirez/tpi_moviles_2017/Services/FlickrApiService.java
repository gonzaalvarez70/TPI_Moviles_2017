package com.alvarezramirez.tpi_moviles_2017.Services;

/**
 * Created by niicoor on 3/11/17.
 */

public class FlickrApiService {

    private static final String PEOPLE_FINDBYUSERNAME = "?method=flickr.people.findByUsername";
    private static final String PEOPLE_FINDBYUSERNAME_PARAM ="&username=";
    private static final String PHOTOSETS_GETLIST = "?method=flickr.photosets.getList";
    private static final String PHOTOSETS_GETLIST_PARAM ="&user_id=";
    private static final String PHOTOSETS_GETPHOTOS_PARAM_USER ="&user_id=";
    private static final String PHOTOSETS_GETPHOTOS = "?method=flickr.photosets.getPhotos";
    private static final String PHOTOSETS_GETPHOTOS_PARAM_PHOTOSET ="&photoset_id=";
    private static final String COMMENTS_GETLIST = "?method=flickr.photos.comments.getList";
    private static final String COMMENTS_GETLIST_PARAM = "&photo_id=";
    private static final String PHOTO_GETINFO = "?method=flickr.photos.getInfo";
    private static final String PHOTO_GETINFO_PARAM = "&photo_id=";

    private static final String BASE_URL = "https://api.flickr.com/services/rest/";

    private static final String API_KEY = "&api_key=54a0c5122f163875d5a0d1e422e8156b";

    private static final String API_SECRET = "1868daf7a14cd1bb";

    private static final String URL_FORMAT = "&format=json&nojsoncallback=1";

    private static FlickrApiService instance = new FlickrApiService();

    private FlickrApiService() {
    }

    public static FlickrApiService getInstance() {
        return instance;
    }

    public String getUserIdByUserNameURL ( String userName){
        String url = "";
        if (userName != null){
            url = BASE_URL + PEOPLE_FINDBYUSERNAME + API_KEY + PEOPLE_FINDBYUSERNAME_PARAM + userName   +URL_FORMAT;
        }
        return url;
    }
    public String getAlbumsFromUserURL ( String userNsid){
        String url = "";
        if (userNsid != null){
            url =  BASE_URL + PHOTOSETS_GETLIST + API_KEY + PHOTOSETS_GETLIST_PARAM + userNsid  + URL_FORMAT;
        }
        return url;
    }
    public String getPhotoSet(String photoSetId, String userId) {
        String url = "";
        if (photoSetId != null & userId != null) {
            url = BASE_URL + PHOTOSETS_GETPHOTOS + API_KEY + PHOTOSETS_GETPHOTOS_PARAM_PHOTOSET + photoSetId +
                    PHOTOSETS_GETPHOTOS_PARAM_USER + userId + URL_FORMAT;
        }
        return url;
    }
    public String getPhotoURL (String farm, String serverId, String photoId, String secret){
        String url = "";
        if (farm != null & serverId != null & photoId != null & secret != null){
            url = "https://farm"+farm+".staticflickr.com/"+serverId+"/"+photoId+"_"+secret+".jpg";
        }
        return url;
    }
    public String getPhotoInfo (String photoId){
        String url = "";
        if (photoId != null) {
            url = BASE_URL + PHOTO_GETINFO + API_KEY + PHOTO_GETINFO_PARAM + photoId + URL_FORMAT;
        }
        return url;
    }
    public String getPhotoComments (String photoId){
        String url = "";
        if (photoId != null) {
            url = BASE_URL + COMMENTS_GETLIST + API_KEY + COMMENTS_GETLIST_PARAM + photoId + URL_FORMAT;
        }
        return url;
    }





}
