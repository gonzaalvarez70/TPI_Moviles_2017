package com.alvarezramirez.tpi_moviles_2017.dao;

import android.content.Context;

import com.alvarezramirez.tpi_moviles_2017.dao.databaseimp.CommentaryDataBaseDao;

/**
 * Created by niicoor on 18/11/17.
 */

public class CommentaryDaoFactory {
        private static CommentaryDao commentaryDaoInstance;

        public static CommentaryDao createCommentaryDaoInstance(Context context) {
            if (commentaryDaoInstance == null) {
                commentaryDaoInstance = new CommentaryDataBaseDao(context);
            }
            return commentaryDaoInstance;
        }


}
