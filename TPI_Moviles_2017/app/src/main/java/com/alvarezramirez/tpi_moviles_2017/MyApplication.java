package com.alvarezramirez.tpi_moviles_2017;

import android.app.Application;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.alvarezramirez.tpi_moviles_2017.Services.FlickrApiService;
import com.alvarezramirez.tpi_moviles_2017.Services.StorageService;
import com.alvarezramirez.tpi_moviles_2017.dao.AlbumDao;
import com.alvarezramirez.tpi_moviles_2017.dao.AlbumDaoFactory;
import com.alvarezramirez.tpi_moviles_2017.dao.CommentaryDao;
import com.alvarezramirez.tpi_moviles_2017.dao.CommentaryDaoFactory;
import com.alvarezramirez.tpi_moviles_2017.dao.PhotoDao;
import com.alvarezramirez.tpi_moviles_2017.dao.PhotoDaoFactory;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by gonza on 15/11/2017.
 */

public class MyApplication extends Application {
    private static RequestQueue queue;
    private static FlickrApiService urlService;
    private static ImageLoader imageLoader;
    @Override
    public  void onCreate(){
        super.onCreate();
        loadAtStartUp();

        queue = Volley.newRequestQueue(this);
        urlService = FlickrApiService.getInstance();
        imageLoader = new ImageLoader(queue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });

    }

    private void loadAtStartUp() {
        CommentaryDao commentaryDao = CommentaryDaoFactory.createCommentaryDaoInstance(this);
        AlbumDao albumDao = AlbumDaoFactory.createAlbumDaoInstance(this);
        PhotoDao photoDao = PhotoDaoFactory.createPhotoDaoInstance(this);
        StorageService.getInstance().load(commentaryDao, albumDao, photoDao);


    }


    public static FlickrApiService getUrlService() {
        return urlService;
    }

    public static ImageLoader getImageLoader() {
        return imageLoader;
    }

    public static RequestQueue getSharedQueue() {
        return queue;
    }


}
