package com.alvarezramirez.tpi_moviles_2017.Entities;

import java.util.List;

/**
 * Created by gonza on 17/11/2017.
 */

public class AlbumPhotos {

    /**
     * photoset : {"id":"72157681235277884","primary":"34082227174","owner":"155153998@N06","ownername":"NiicooR","photo":[{"id":"34082227174","secret":"d49fe2d36b","server":"4268","farm":5,"title":"IMG-20170526-WA0002","isprimary":1,"ispublic":1,"isfriend":0,"isfamily":0}],"page":1,"per_page":"500","perpage":"500","pages":1,"total":1,"title":"Test Album"}
     * stat : ok
     */

    private PhotosetBean photoset;
    private String stat;

    public PhotosetBean getPhotoset() {
        return photoset;
    }

    public void setPhotoset(PhotosetBean photoset) {
        this.photoset = photoset;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public static class PhotosetBean {
        /**
         * id : 72157681235277884
         * primary : 34082227174
         * owner : 155153998@N06
         * ownername : NiicooR
         * photo : [{"id":"34082227174","secret":"d49fe2d36b","server":"4268","farm":5,"title":"IMG-20170526-WA0002","isprimary":1,"ispublic":1,"isfriend":0,"isfamily":0}]
         * page : 1
         * per_page : 500
         * perpage : 500
         * pages : 1
         * total : 1
         * title : Test Album
         */

        private String id;
        private String primary;
        private String owner;
        private String ownername;
        private int page;
        private String per_page;
        private String perpage;
        private int pages;
        private int total;
        private String title;
        private List<PhotoBean> photo;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPrimary() {
            return primary;
        }

        public void setPrimary(String primary) {
            this.primary = primary;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public String getOwnername() {
            return ownername;
        }

        public void setOwnername(String ownername) {
            this.ownername = ownername;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public String getPer_page() {
            return per_page;
        }

        public void setPer_page(String per_page) {
            this.per_page = per_page;
        }

        public String getPerpage() {
            return perpage;
        }

        public void setPerpage(String perpage) {
            this.perpage = perpage;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<PhotoBean> getPhoto() {
            return photo;
        }

        public void setPhoto(List<PhotoBean> photo) {
            this.photo = photo;
        }

        public static class PhotoBean {
            /**
             * id : 34082227174
             * secret : d49fe2d36b
             * server : 4268
             * farm : 5
             * title : IMG-20170526-WA0002
             * isprimary : 1
             * ispublic : 1
             * isfriend : 0
             * isfamily : 0
             */

            private String id;
            private String secret;
            private String server;
            private int farm;
            private String title;
            private int isprimary;
            private int ispublic;
            private int isfriend;
            private int isfamily;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getSecret() {
                return secret;
            }

            public void setSecret(String secret) {
                this.secret = secret;
            }

            public String getServer() {
                return server;
            }

            public void setServer(String server) {
                this.server = server;
            }

            public int getFarm() {
                return farm;
            }

            public void setFarm(int farm) {
                this.farm = farm;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getIsprimary() {
                return isprimary;
            }

            public void setIsprimary(int isprimary) {
                this.isprimary = isprimary;
            }

            public int getIspublic() {
                return ispublic;
            }

            public void setIspublic(int ispublic) {
                this.ispublic = ispublic;
            }

            public int getIsfriend() {
                return isfriend;
            }

            public void setIsfriend(int isfriend) {
                this.isfriend = isfriend;
            }

            public int getIsfamily() {
                return isfamily;
            }

            public void setIsfamily(int isfamily) {
                this.isfamily = isfamily;
            }
        }
    }
}
