package com.alvarezramirez.tpi_moviles_2017;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alvarezramirez.tpi_moviles_2017.Entities.AlbumsDirectory;
import com.alvarezramirez.tpi_moviles_2017.Entities.AlbumsDirectory.PhotosetsBean.PhotosetBean;
import com.alvarezramirez.tpi_moviles_2017.Model.Album;
import com.alvarezramirez.tpi_moviles_2017.Services.FlickrApiService;
import com.alvarezramirez.tpi_moviles_2017.Services.StorageService;
import com.alvarezramirez.tpi_moviles_2017.dao.AlbumDao;
import com.alvarezramirez.tpi_moviles_2017.dao.AlbumDaoFactory;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class UserPhotoSetActivity extends ListActivity {
    private String idUser;
    private FlickrApiService urlService;
    private Gson gson;
    private AlbumsDirectory albumsDirectory;
    private UserPhotoSetActivity.PhotoSetAdapter photoSetAdapter;
    private AlbumDao albumDao = StorageService.getInstance().getAlbumDao();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_photo_set);
        urlService = MyApplication.getUrlService();
        idUser = this.getIntent().getStringExtra("idUser");//
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
        this.loadPhotoSets(idUser);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getBaseContext(), AlbumActivity.class);
                intent.putExtra("idUser",idUser);
                intent.putExtra("idPhotoSet",((Album)parent.getItemAtPosition(position)).getId());

                startActivity(intent);
            }
        });
    }

    private void loadPhotoSets(String idUser){
        String url = urlService.getAlbumsFromUserURL(idUser);
        StringRequest request = new StringRequest(Request.Method.GET, url,
                onPhotoSetsLoaded, onPhotoSetsError);
        MyApplication.getSharedQueue().add(request);
    }
    private final Response.Listener<String> onPhotoSetsLoaded = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Toast toast;
            albumsDirectory = gson.fromJson(response, AlbumsDirectory.class);
            photoSetAdapter = new UserPhotoSetActivity.PhotoSetAdapter();
            List<Album> photoSets = parseUserPhotosetsToPhotoSetFlickrList(albumsDirectory);
            photoSetAdapter.setPhotoSets(photoSets);
            setListAdapter(photoSetAdapter);
            toast = Toast.makeText(UserPhotoSetActivity.this, "Se actualizarán los Photosets almacenadas", Toast.LENGTH_SHORT);
            toast.show();
            int bdIndex = albumDao.addAlbums(photoSets);
            if (bdIndex != -1){
                toast = Toast.makeText(UserPhotoSetActivity.this, "Photosets almacenados correctamente", Toast.LENGTH_SHORT);
                toast.show();
            }
            else
            {
                toast = Toast.makeText(UserPhotoSetActivity.this, "No se ha podido actualizar la Base de datos de Photosets", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    };

    private final Response.ErrorListener onPhotoSetsError = new Response.ErrorListener(){

        @Override
        public void onErrorResponse(VolleyError error) {
            System.out.print(error.getMessage());
            Toast toast;
            toast = Toast.makeText(UserPhotoSetActivity.this, "No hay conexión. Se recuperará la info de Photosets almacenados", Toast.LENGTH_SHORT);
            toast.show();
            List<Album> photoSets = albumDao.getAlbums();
            if (photoSets.size()!= 0){
                photoSetAdapter = new PhotoSetAdapter();
                photoSetAdapter.setPhotoSets(photoSets);
                setListAdapter(photoSetAdapter);
                toast = Toast.makeText(UserPhotoSetActivity.this, "Info de Photosets recuperada Correctamente", Toast.LENGTH_SHORT);
                toast.show();
            }
            else{
                toast = Toast.makeText(UserPhotoSetActivity.this, "No hay Photosets en base de datos", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    };

    private List<Album> parseUserPhotosetsToPhotoSetFlickrList(AlbumsDirectory photosets){

        List<PhotosetBean> photoSets = photosets.getPhotosets().getPhotoset();

        List<Album> photoSetsFlickr = new ArrayList<>();
        Album album;
        for ( PhotosetBean photoSet : photoSets) {
            album = new Album();
            album.setTitle(photoSet.getTitle().get_content());
            album.setId(photoSet.getId());
            album.setFarm(String.valueOf(photoSet.getFarm()));
            album.setPrimary(photoSet.getPrimary());
            album.setSecret(photoSet.getSecret());
            album.setServer(photoSet.getServer());
            album.setPhotos(photoSet.getPhotos());
            photoSetsFlickr.add(album);
        }
        return photoSetsFlickr;
    }

    private class PhotoSetAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {


        private ArrayList<Album> list;
        private LayoutInflater inflater;

        public PhotoSetAdapter() {
            this.list = new ArrayList();
            this.inflater = LayoutInflater.from(getBaseContext());
        }

        public void setPhotoSets( List<Album> photoSets){
            list.clear();
            list.addAll(photoSets);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View item = convertView != null ? convertView : inflater.inflate(R.layout.photoset_item_view, null);
            UserPhotoSetActivity.PhotoSetAdapter.Holder holder = null;
            if(item.getTag() == null){
                TextView title = (TextView) item.findViewById(R.id.photosetTitle);
                ImageView photo = (ImageView) item.findViewById(R.id.primaryPhoto);
                holder = new UserPhotoSetActivity.PhotoSetAdapter.Holder();
                holder.photo = photo;
                holder.title = title;
                item.setTag(holder);
            }
            else
            {
                holder = (UserPhotoSetActivity.PhotoSetAdapter.Holder) item.getTag();
            }
            this.loadImage(position, holder.photo);
            holder.title.setText(((Album)getItem(position)).getTitle());
            return item;

        }
        private void loadImage(int position, final ImageView photo){
            ImageLoader imageLoader = MyApplication.getImageLoader();
            String farm = ((Album)getItem(position)).getFarm();
            String server =((Album)getItem(position)).getServer();
            String idPhoto = ((Album)getItem(position)).getPrimary();
            String secret = ((Album)getItem(position)).getSecret();
            String url = urlService.getPhotoURL(farm,server,idPhoto,secret);
            imageLoader.get(url, new ImageLoader.ImageListener() {

                public void onErrorResponse(VolleyError error) {
                    System.out.print("Error: " + error.getMessage());
                }
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        photo.setImageBitmap(response.getBitmap());
                    }
                }
            });
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }

        private class Holder {
            public ImageView photo;
            public TextView title;
        }
    }
}