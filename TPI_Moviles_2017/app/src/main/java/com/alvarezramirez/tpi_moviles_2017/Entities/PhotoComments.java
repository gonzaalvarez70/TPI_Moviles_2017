package com.alvarezramirez.tpi_moviles_2017.Entities;

import java.util.List;

/**
 * Created by gonza on 15/11/2017.
 */

public class PhotoComments {

    /**
     * comments : {"photo_id":"34082227174","comment":[{"id":"155108676-34082227174-72157662673372988","author":"151193069@N04","author_is_deleted":0,"authorname":"gonzaalvarez70","iconserver":0,"iconfarm":0,"datecreate":"1510777958","permalink":"https://www.flickr.com/photos/155153998@N06/34082227174/#comment72157662673372988","path_alias":"","realname":"Gonzalo Alvarez","_content":"Testeando el comentario equisde"},{"id":"155108676-34082227174-72157689615216575","author":"151193069@N04","author_is_deleted":0,"authorname":"gonzaalvarez70","iconserver":0,"iconfarm":0,"datecreate":"1510780319","permalink":"https://www.flickr.com/photos/155153998@N06/34082227174/#comment72157689615216575","path_alias":"","realname":"Gonzalo Alvarez","_content":"otro comment xd"}]}
     * stat : ok
     */

    private CommentsBean comments;
    private String stat;

    public CommentsBean getComments() {
        return comments;
    }

    public void setComments(CommentsBean comments) {
        this.comments = comments;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public static class CommentsBean {
        /**
         * photo_id : 34082227174
         * comment : [{"id":"155108676-34082227174-72157662673372988","author":"151193069@N04","author_is_deleted":0,"authorname":"gonzaalvarez70","iconserver":0,"iconfarm":0,"datecreate":"1510777958","permalink":"https://www.flickr.com/photos/155153998@N06/34082227174/#comment72157662673372988","path_alias":"","realname":"Gonzalo Alvarez","_content":"Testeando el comentario equisde"},{"id":"155108676-34082227174-72157689615216575","author":"151193069@N04","author_is_deleted":0,"authorname":"gonzaalvarez70","iconserver":0,"iconfarm":0,"datecreate":"1510780319","permalink":"https://www.flickr.com/photos/155153998@N06/34082227174/#comment72157689615216575","path_alias":"","realname":"Gonzalo Alvarez","_content":"otro comment xd"}]
         */

        private String photo_id;
        private List<CommentBean> comment;

        public String getPhoto_id() {
            return photo_id;
        }

        public void setPhoto_id(String photo_id) {
            this.photo_id = photo_id;
        }

        public List<CommentBean> getComment() {
            return comment;
        }

        public void setComment(List<CommentBean> comment) {
            this.comment = comment;
        }

        public static class CommentBean {
            /**
             * id : 155108676-34082227174-72157662673372988
             * author : 151193069@N04
             * author_is_deleted : 0
             * authorname : gonzaalvarez70
             * iconserver : 0
             * iconfarm : 0
             * datecreate : 1510777958
             * permalink : https://www.flickr.com/photos/155153998@N06/34082227174/#comment72157662673372988
             * path_alias :
             * realname : Gonzalo Alvarez
             * _content : Testeando el comentario equisde
             */

            private String id;
            private String author;
            private int author_is_deleted;
            private String authorname;
            private int iconserver;
            private int iconfarm;
            private String datecreate;
            private String permalink;
            private String path_alias;
            private String realname;
            private String _content;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getAuthor() {
                return author;
            }

            public void setAuthor(String author) {
                this.author = author;
            }

            public int getAuthor_is_deleted() {
                return author_is_deleted;
            }

            public void setAuthor_is_deleted(int author_is_deleted) {
                this.author_is_deleted = author_is_deleted;
            }

            public String getAuthorname() {
                return authorname;
            }

            public void setAuthorname(String authorname) {
                this.authorname = authorname;
            }

            public int getIconserver() {
                return iconserver;
            }

            public void setIconserver(int iconserver) {
                this.iconserver = iconserver;
            }

            public int getIconfarm() {
                return iconfarm;
            }

            public void setIconfarm(int iconfarm) {
                this.iconfarm = iconfarm;
            }

            public String getDatecreate() {
                return datecreate;
            }

            public void setDatecreate(String datecreate) {
                this.datecreate = datecreate;
            }

            public String getPermalink() {
                return permalink;
            }

            public void setPermalink(String permalink) {
                this.permalink = permalink;
            }

            public String getPath_alias() {
                return path_alias;
            }

            public void setPath_alias(String path_alias) {
                this.path_alias = path_alias;
            }

            public String getRealname() {
                return realname;
            }

            public void setRealname(String realname) {
                this.realname = realname;
            }

            public String get_content() {
                return _content;
            }

            public void set_content(String _content) {
                this._content = _content;
            }
        }
    }
}
