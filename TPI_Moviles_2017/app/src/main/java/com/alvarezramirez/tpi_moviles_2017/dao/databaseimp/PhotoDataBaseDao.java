package com.alvarezramirez.tpi_moviles_2017.dao.databaseimp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alvarezramirez.tpi_moviles_2017.Model.Photo;
import com.alvarezramirez.tpi_moviles_2017.dao.PhotoDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by niicoor on 19/11/17.
 */

public class PhotoDataBaseDao implements PhotoDao {

    private FlickrSqlDataBaseHelper flickrSqlDataBaseHelper;

    public PhotoDataBaseDao(Context context) {
        this.flickrSqlDataBaseHelper = FlickrSqlDataBaseHelper.getInstance(context);
    }

    @Override
    public List<Photo> getPhotos(String idPhotoSet) {
        SQLiteDatabase db = flickrSqlDataBaseHelper.getReadableDatabase();
        Cursor cursor = db.query("photo", null, null,null,null,null,null);
        List<Photo> photos = null;
        if (cursor.isBeforeFirst()) {
            Photo photo;
            int indexIdPhoto;
            int indexSecret;
            int indexServer;
            int indexFarm;
            int indexTitle;
            int indexDate;
            int indexIdAlbum;
            photos = new ArrayList<>();
            indexIdPhoto =  cursor.getColumnIndex("idPhoto");
            indexSecret = cursor.getColumnIndex("secret");
            indexServer = cursor.getColumnIndex("server");
            indexFarm = cursor.getColumnIndex("farm");
            indexTitle = cursor.getColumnIndex("title");
            indexDate = cursor.getColumnIndex("date");
            indexIdAlbum = cursor.getColumnIndex("idAlbum");

            while (cursor.moveToNext()) {
                if(cursor.getString(indexIdAlbum).compareTo(idPhotoSet)==0){
                    photo = new Photo();
                    photo.setIdPhoto(cursor.getString(indexIdPhoto));
                    photo.setSecret(cursor.getString(indexSecret));
                    photo.setServer(cursor.getString(indexServer));
                    photo.setFarm(cursor.getString(indexFarm));
                    photo.setTitle(cursor.getString(indexTitle));
                    photo.setDate(cursor.getString(indexDate));
                    photos.add(photo);
                }

            }
        }
        cursor.close();
        db.close();
        return photos;
    }

    @Override
    public boolean deletePhotos(String idPhotoSet) {
        SQLiteDatabase db = flickrSqlDataBaseHelper.getWritableDatabase();
        int updated = db.delete("photo", null, null);
        db.close();
        return updated != 0;
    }

    @Override
    public int addPhotos(List<Photo> photos,String idPhotoSet) {
        //Para que no se acumulen fotos de diferentes albumes.
        //this.deletePhotos();
        SQLiteDatabase db = flickrSqlDataBaseHelper.getWritableDatabase();
        int lastIndex = -1;
        for (Photo photo: photos) {
            ContentValues values = new ContentValues();
            values.put("idPhoto"    , photo.getIdPhoto());
            values.put("secret"     , photo.getSecret());
            values.put("server"     , photo.getServer());
            values.put("farm"       , photo.getFarm());
            values.put("title"      , photo.getTitle());
            values.put("date"       , photo.getDate());
            values.put("idAlbum", idPhotoSet);
            lastIndex = (int)db.insert("photo", null, values);
        }
        db.close();
        return lastIndex;
    }
}
