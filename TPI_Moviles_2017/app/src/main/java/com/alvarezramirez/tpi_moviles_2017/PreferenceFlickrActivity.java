package com.alvarezramirez.tpi_moviles_2017;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;

import java.util.Locale;


public class PreferenceFlickrActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        Preference.OnPreferenceChangeListener listener = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                // newValue es el nuevo valor que se selecciona
                Locale locale = new Locale("es", "ES");;
                if(Integer.parseInt(newValue.toString()) == 1){
                    locale = new Locale("es", "ES");
                }else if(Integer.parseInt(newValue.toString()) == 2){
                    locale = Locale.ENGLISH;
                }
                Resources resources = getBaseContext().getResources();
                Configuration configuration = resources.getConfiguration();
                configuration.setLocale(locale);
                DisplayMetrics dm = resources.getDisplayMetrics();
                //getBaseContext().createConfigurationContext(configuration);
                resources.updateConfiguration(configuration,dm);

                finish();
                startActivity(new Intent(getBaseContext(), PreferenceFlickrActivity.class));
                overridePendingTransition(0, 0);

                return true;
            }
        };
       ListPreference listPreference = (ListPreference)findPreference("listPref");
       // listPreference.setValueIndex(1);
        listPreference.setOnPreferenceChangeListener(listener);

    }

}

